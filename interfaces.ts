export interface User {
  uid: string;
  email: string;
  sections: Section[];
}

export interface Section {
  uid: string;
  name: string;
  children: Group[];
  folded?: boolean;
  parent?: string;
}

export interface Group {
  uid: string;
  name: string;
  parent: string;
}

export interface Snippet {
  uid: string;
  name: string;
  description: string;
  language: string;
  body: string;
  parentID: string;
}

export interface SearchSnippet {
  uid: string;
  name: string;
  description: string;
  parentID: string;
}
