import { Component, ViewChild, ElementRef } from "@angular/core";
import { RouterOutlet } from "@angular/router";
import { RouteAnimations } from "./animations";
import { CopyService } from "./services/copy.service";

@Component({
  selector: "app-root",
  template: `
    <div class="page" [@routeAnimation]="getDepth(myOutlet)">
      <router-outlet #myOutlet="outlet"></router-outlet>
    </div>

    <div class="copy" #copy>COPIED!</div>
  `,
  styleUrls: ["./app.component.sass"],
  animations: RouteAnimations,
})
export class AppComponent {
  @ViewChild("copy")
  copyElem: ElementRef<HTMLDivElement>;

  constructor(private copyServ: CopyService) {}

  ngOnInit() {
    this.copyServ.copied$.subscribe(() => {
      //show copy popup
      const copy = this.copyElem.nativeElement;
      copy.classList.remove("active");
      void copy.offsetWidth;
      setTimeout(() => {
        copy.classList.add("active");
      }, 0);
    });
  }
  //-------ROUTER ANIMS------
  getDepth(outlet: RouterOutlet) {
    let name = outlet.activatedRouteData["name"];
    if (name) return name;
    else return -1;
  }
}
