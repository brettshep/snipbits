import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { SortablejsModule } from "ngx-sortablejs";
import { Store } from "./store";

// Firebase
import { AngularFireModule } from "@angular/fire";
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { AngularFireAuthModule } from "@angular/fire/auth";

const config = {
  apiKey: "AIzaSyCveJ3nrvDwm0Kpx9rQbjUqW7Wzsrx_3J8",
  authDomain: "snipbits-f8f5f.firebaseapp.com",
  databaseURL: "https://snipbits-f8f5f.firebaseio.com",
  projectId: "snipbits-f8f5f",
  storageBucket: "snipbits-f8f5f.appspot.com",
  messagingSenderId: "689339181809",
  appId: "1:689339181809:web:f3626e1519043d51d21750",
  measurementId: "G-FJYF9H79N3",
};

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(config),
    SortablejsModule.forRoot({}),
  ],
  providers: [Store],
  bootstrap: [AppComponent],
})
export class AppModule {}
