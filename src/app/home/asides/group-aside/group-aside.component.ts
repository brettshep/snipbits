import { Group, Section, User } from "../../../../../interfaces";
import {
  Component,
  Input,
  Output,
  EventEmitter,
  ViewChildren,
  QueryList,
  ElementRef,
} from "@angular/core";
import { Options } from "sortablejs";
import { SectionsService } from "../../../services/sections.service";

@Component({
  selector: "group-aside",
  templateUrl: "./group-aside.component.html",
  styleUrls: ["./group-aside.component.sass"],
})
export class GroupAsideComponent {
  activeItemID: string = "ALL";
  contextPos: { x: number; y: number };
  contextItem: Group | Section;
  renameItem: Group | Section;
  dragItem: Group | Section;
  sections: Section[];
  draggingSnippet: boolean = false;
  dblClickTime: number = 0;
  dblClickItemID: string;
  readonly maxDblClickTime: number = 400;
  // dblClickTimeout: any;
  /* #region  ---Sortablejs Options */
  sortableSectionOptions: Options = {
    group: "group",
    swapThreshold: 0.5,
    ghostClass: "drag",
    animation: 150,
    onStart: this.sortableDragStart.bind(this),
    onEnd: this.sortableDragEnd.bind(this),
  };

  sortableGroupOptions: Options = {
    group: "group2",
    swapThreshold: 1,
    ghostClass: "drag",
    animation: 150,
    onStart: this.sortableDragStart.bind(this),
    onEnd: this.sortableDragEnd.bind(this),
  };
  /* #endregion */

  @Input() set setSections(user: User) {
    this.sections = user.sections;
  }

  @Input() draggingSnippetEmitter: EventEmitter<any>;

  @Output() onUpdate = new EventEmitter<{
    sections: Section[];
    choosenID: string;
  }>();

  @Output() chooseCategory = new EventEmitter<string>();

  @Output() changeSnippetParent = new EventEmitter<{
    snipID: string;
    parent: Section | Group;
  }>();

  @ViewChildren("draggable") draggables: QueryList<ElementRef<HTMLElement>>;

  constructor(private sectionServ: SectionsService) {}

  ngOnInit() {
    this.draggingSnippetEmitter.subscribe((val) => {
      this.draggingSnippet = val;
    });
  }

  setActiveItemID(item: Section | Group | "ALL") {
    // set ALL
    if (typeof item === "string") {
      this.activeItemID = item;
      this.chooseCategory.emit(item);
      return;
    } else {
      // on click
      this.activeItemID = item.uid;
      this.chooseCategory.emit(item.uid);

      // check for doubleclick
      if (this.dblClickItemID === item.uid) {
        if (
          Date.now() - this.dblClickTime < this.maxDblClickTime &&
          this.dblClickItemID
        ) {
          this.renameItem = { ...item };
          this.dblClickTime = 0;
          this.dblClickItemID = "";
        }
      }

      //  dblclick
      this.dblClickItemID = item.uid;
      this.dblClickTime = Date.now();
    }
  }
  // // set ALL
  // if (typeof item === "string") {
  //   this.activeItemID = item;
  //   this.chooseCategory.emit(item);
  //   return;
  // }

  // // check for doubleclick
  // if (
  //   Date.now() - this.dblClickTime < this.maxDblClickTime &&
  //   this.dblClickItemID &&
  //   this.dblClickItemID === item.uid
  // ) {
  //   this.renameItem = { ...item };
  //   this.dblClickTime = 0;
  //   this.dblClickItemID = "";
  //   clearTimeout(this.dblClickTimeout);
  // } else {
  //   // else first click
  //   this.dblClickItemID = item.uid;
  //   this.dblClickTime = Date.now();
  //   this.dblClickTimeout = setTimeout(() => {
  //     // on click
  //     this.activeItemID = item.uid;
  //     this.chooseCategory.emit(item.uid);
  //     // reset dblClickTime
  //     this.dblClickTime = 0;
  //   }, this.maxDblClickTime);
  // }

  /* #region  ---Sortable Events */

  sortableDragStart(e: any) {
    // add target drag class
    const target: HTMLElement =
      (<HTMLElement>e.item).nodeName === "FORM"
        ? e.item
        : (<HTMLElement>e.item).querySelector(".section-name");

    target.classList.add("drag");

    // add freexr class to all other items
    this.draggables.forEach((item) => {
      if (item.nativeElement !== target)
        item.nativeElement.classList.add("freeze");
    });

    // set dragitem
    const ID = target.dataset.id;
    this.dragItem = this.getItemByID(ID);
  }

  sortableDragEnd(e: any) {
    // remove target drag class
    const target: HTMLElement =
      (<HTMLElement>e.item).nodeName === "FORM"
        ? e.item
        : (<HTMLElement>e.item).querySelector(".section-name");
    target.classList.remove("drag");

    // remove freeze on mousemove (prevent wonky css hover effect)
    const func = () => {
      this.draggables.forEach((item) => {
        item.nativeElement.classList.remove("freeze");
      });
      document.removeEventListener("mousemove", func);
    };
    document.addEventListener("mousemove", func);

    // change item parent class if parent
    if (this.dragItem.parent) {
      this.resetGroupParents();
    }
    this.dragItem = null;
    this.onUpdate.emit({
      sections: this.sections,
      choosenID: this.activeItemID,
    });
  }

  /* #endregion */

  /* #region  ---Item CRUD Operations */
  deleteItem(item?: Group | Section) {
    const deleteItem = item ? item : this.contextItem;
    // if sub-group
    if (deleteItem.parent) {
      const parent = this.sections.find(
        (section) => section.uid === deleteItem.parent
      );

      // remove from children
      parent.children = parent.children.filter(
        (group) => group.uid !== deleteItem.uid
      );
    }
    //  base-group
    else {
      // remove from section array
      this.sections = this.sections.filter(
        (group) => group.uid !== deleteItem.uid
      );
    }

    this.onUpdate.emit({
      sections: this.sections,
      choosenID: this.activeItemID,
    });
  }

  changeItemName(name: string) {
    if (!this.renameItem) return;

    // find item and set name
    let item: Section | Group;

    // is a group
    if (this.renameItem.parent) {
      // find parent
      const parentSection = this.sections.find(
        (section) => section.uid === this.renameItem.parent
      );
      // find group in parent
      item = parentSection.children.find(
        (group) => group.uid === this.renameItem.uid
      );
    }
    // is a section
    else {
      item = this.sections.find(
        (section) => section.uid === this.renameItem.uid
      );
    }

    item.name = name;

    // clear namechange var
    this.renameItem = null;

    this.onUpdate.emit({
      sections: this.sections,
      choosenID: this.activeItemID,
    });
  }

  createNewItem(e: Event, parentID?: string) {
    e.preventDefault();
    e.stopPropagation();

    let item: Section | Group;

    // add group to section
    if (parentID) {
      const newGroup: Group = {
        uid: this.sectionServ.newUID,
        parent: parentID ? parentID : null,
        name: "Untitled",
      };

      item = newGroup;

      // find parent and add to children array
      const parentSection = this.sections.find(
        (section) => section.uid === parentID
      );

      parentSection.children = [...parentSection.children, newGroup];

      // unfold
      parentSection.folded = false;
    }
    // add root group
    else {
      const newSection: Section = {
        uid: Math.random().toString(),
        name: "Untitled",
        folded: false,
        children: [],
      };

      item = newSection;

      this.sections = [...this.sections, newSection];
    }

    // set current rename
    this.renameItem = item;

    this.onUpdate.emit({
      sections: this.sections,
      choosenID: this.activeItemID,
    });
  }

  /* #endregion */

  /* #region  ---Context Menu Toggle and Events */
  openContextMenu(e: MouseEvent, item: Group | Section) {
    e.preventDefault();
    this.contextItem = { ...item };

    const baseCords = {
      x: e.clientX,
      y: e.clientY,
    };

    const newCords = { ...baseCords };

    const menuHeight = item.parent ? 136 + 16 : 96 + 16;

    if (window.innerHeight - baseCords.y < menuHeight) {
      newCords.y = window.innerHeight - menuHeight;
    }

    this.contextPos = newCords;
  }

  closeContextMenu() {
    this.contextPos = null;
    this.contextItem = null;
  }

  setRenameItemContextEvent() {
    this.renameItem = { ...this.contextItem };
  }

  /* #endregion */

  /* #region  --Dom Events */
  formNameChangeSubmit(e: Event) {
    e.preventDefault();
  }

  onSnippetDrop(e: DragEvent, target: Section | Group) {
    if (!this.draggingSnippet) return;
    const id = e.dataTransfer.getData("id");

    this.changeSnippetParent.emit({ snipID: id, parent: target });

    this.onSnippetDragout(e);
  }

  onSnippetDragover(e: DragEvent) {
    if (!this.draggingSnippet) return;
    const target: HTMLElement =
      (<HTMLElement>e.target).nodeName === "FORM"
        ? <HTMLElement>e.target
        : (<HTMLElement>e.target).parentElement;

    target.classList.add("drag");

    e.preventDefault();
  }

  onSnippetDragout(e: DragEvent) {
    if (!this.draggingSnippet) return;
    const target: HTMLElement = <HTMLElement>e.target;
    target.classList.remove("drag");
  }

  /* #endregion */

  /* #region  ---Utilities */

  resetGroupParents() {
    for (let i = 0; i < this.sections.length; i++) {
      const section = this.sections[i];
      for (let j = 0; j < section.children.length; j++) {
        const child = section.children[j];
        child.parent = section.uid;
      }
    }
  }

  getItemByID(id: string): Group | Section {
    for (let i = 0; i < this.sections.length; i++) {
      const section = this.sections[i];
      if (section.uid === id) {
        return section;
      } else {
        for (let j = 0; j < section.children.length; j++) {
          const child = section.children[j];
          if (child.uid === id) {
            return child;
          }
        }
      }
    }
  }
  /* #endregion */
}
