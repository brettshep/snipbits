import {
  Snippet,
  SearchSnippet,
  Section,
  User,
} from "../../../../../interfaces";
import {
  Component,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
} from "@angular/core";
@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "snippet-aside",
  template: `
    <aside>
      <div class="scroll">
        <div class="search">
          <i class="fas fa-search"></i>
          <input
            #search
            type="text"
            placeholder="Search..."
            (input)="filterSnippets()"
          />
        </div>

        <div
          class="snippet"
          draggable="true"
          (dragstart)="setDragData($event, snippet.uid)"
          (dragend)="draggingSnippetEmitter.emit(false)"
          [class.active]="currSnippet?.uid === snippet.uid"
          *ngFor="let snippet of filteredSnippets"
          (click)="setCurrSnip(snippet.uid)"
        >
          <h2>{{ snippet.name }}</h2>
          <h3>{{ snippet.description }}</h3>
        </div>
      </div>
      <!-- new snippet button -->
      <div class="new-snip-cont">
        <!-- gradient -->
        <div class="grad-overlay"></div>
        <!-- button -->
        <div class="new-snip-button-cont">
          <button class="new-snip-btn" (click)="newSnippet.emit()">
            <i class="fas fa-plus"></i>
            New Snippet
          </button>
        </div>
      </div>
    </aside>
  `,
  styleUrls: ["./snippet-aside.component.sass"],
})
export class SnippetAsideComponent {
  @Input() currSnippet: Snippet;
  @Input() draggingSnippetEmitter: EventEmitter<any>;
  @Input() set setSections(user: User) {
    this.sections = user.sections;
  }

  @Input() set setSnippets(val: SearchSnippet[]) {
    if (val) {
      this.searchSnippets = val;
      this.filterSnippets();
    } else {
      this.searchSnippets = [];
      this.filterSnippets();
    }
  }

  @ViewChild("search", { static: true }) searchRef: ElementRef<
    HTMLInputElement
  >;

  @Output() newSnippet = new EventEmitter();

  @Output() selectSnippet = new EventEmitter<string>();

  searchSnippets: SearchSnippet[];
  filteredSnippets: SearchSnippet[];
  sections: Section[];

  setDragData(e: DragEvent, id: string) {
    e.dataTransfer.setData("id", id);
    this.draggingSnippetEmitter.emit(true);
  }

  setCurrSnip(id: string) {
    this.selectSnippet.emit(id);
  }

  filterSnippets() {
    const input = this.searchRef.nativeElement.value;

    if (input) {
      this.filteredSnippets = this.searchSnippets.filter((snippet) =>
        snippet.name.toLowerCase().includes(input.toLowerCase())
      );
    } else {
      this.filteredSnippets = [...this.searchSnippets];
    }
  }
}
