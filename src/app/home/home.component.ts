import { Component, EventEmitter, ChangeDetectorRef } from "@angular/core";
import { SnippetService } from "../services/snippet.service";
import {
  Group,
  Snippet,
  Section,
  User,
  SearchSnippet,
} from "../../../interfaces";
import { Subject, Observable } from "rxjs";
import { SectionsService } from "../services/sections.service";
import { Store } from "../store";

@Component({
  selector: "home",
  template: `
    <div class="grid" *ngIf="user">
      <group-aside
        [setSections]="user"
        [draggingSnippetEmitter]="draggingSnippetEmitter"
        (onUpdate)="updateSections($event)"
        (chooseCategory)="setNewCategory($event)"
        (changeSnippetParent)="changeSnippetParent($event)"
      ></group-aside>
      <snippet-aside
        [draggingSnippetEmitter]="draggingSnippetEmitter"
        [setSnippets]="searchSnippets"
        [currSnippet]="currSnip$ | async"
        [setSections]="user"
        (newSnippet)="createNewSnippet()"
        (selectSnippet)="selectSnippet($event)"
      ></snippet-aside>
      <div class="snippet-wrapper">
        <snippet-header
          [setSnippet]="currSnip$ | async"
          [resizeEditorEmitter]="resizeEditorEmitter"
          [copyEmitter]="copyEmitter"
          (onSnippetUpdate)="updateSnippetData($event)"
          (deleteSnippet)="deleteSnippet($event)"
        ></snippet-header>
        <snippet-body
          [resizeEditorEmitter]="resizeEditorEmitter"
          [copyEmitter]="copyEmitter"
          [setSnippet]="currSnip$ | async"
          (save)="updateSnippetData($event)"
        ></snippet-body>
        <div *ngIf="!(currSnip$ | async)" class="select-snippet">
          <i class="fas fa-cut"></i>
          Select A Snippet To View or Edit.
        </div>
      </div>
    </div>
  `,
  styleUrls: ["./home.component.sass"],
})
export class HomeComponent {
  constructor(
    private snippetServ: SnippetService,
    private sectionServ: SectionsService,
    private store: Store,
    private cd: ChangeDetectorRef
  ) {}

  resizeEditorEmitter = new EventEmitter();
  copyEmitter = new EventEmitter();
  draggingSnippetEmitter = new EventEmitter();

  user: User;
  searchSnippets: SearchSnippet[];
  currSnip$: Observable<Snippet>;

  ngOnInit() {
    this.currSnip$ = this.store.select("currSnippet");

    this.store.select<User>("user").subscribe((val) => {
      // first user update
      if (val && !this.user) {
        // setup searchsnippet observable
        this.snippetServ.getFilteredSearchSnips().subscribe((val) => {
          this.searchSnippets = val;
          this.cd.detectChanges();
        });
      }
      this.user = val;
    });
  }

  /* #region  ---Snippets */
  selectSnippet(id: string) {
    this.snippetServ.fetchSnippet(id);
  }

  createNewSnippet() {
    this.snippetServ.createNewSnippet();
  }

  updateSnippetData(e) {
    this.snippetServ.updateSnippetData(e.id, e.propName, e.data);
  }

  deleteSnippet(e) {
    this.snippetServ.deleteSnippet(e);
  }

  changeSnippetParent(e: { snipID: string; parent: Section | Group }) {
    this.snippetServ.updateSnippetData(e.snipID, "parentID", e.parent.uid);
  }

  /* #endregion */

  /* #region  ---Sections */
  updateSections(e: { sections: Section[]; choosenID: string }) {
    this.sectionServ.updateSections(e.sections);
    this.snippetServ.choosenCategoryID$.next(e.choosenID);
  }

  setNewCategory(id: string) {
    this.snippetServ.choosenCategoryID$.next(id);
  }
  /* #endregion */
}
