import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SortablejsModule } from "ngx-sortablejs";
import { RouterModule, Routes } from "@angular/router";
import { CodemirrorModule } from "@ctrl/ngx-codemirror";

import { HomeComponent } from "./home.component";
import { GroupAsideComponent } from "./asides/group-aside/group-aside.component";
import { SnippetAsideComponent } from "./asides/snippet-aside/snippet-aside.component";
import { SnippetHeaderComponent } from "./snippet/snippet-header/snippet-header.component";
import { SnippetBodyComponent } from "./snippet/snippet-body/snippet-body.component";
import { ContextMenuComponent } from "./shared/context-menu/context-menu.component";
import { FocusDirective } from "./shared/directives/input-focus.directive";
import { TextAreaDirective } from "./shared/directives/text-area.directive";
import { FormsModule } from "@angular/forms";

export const ROUTES: Routes = [
  {
    path: "",
    component: HomeComponent,
  },
  { path: "**", redirectTo: "/home" },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES),
    SortablejsModule,
    CodemirrorModule,
    FormsModule,
  ],
  declarations: [
    HomeComponent,
    GroupAsideComponent,
    SnippetAsideComponent,
    SnippetHeaderComponent,
    SnippetBodyComponent,
    ContextMenuComponent,
    FocusDirective,
    TextAreaDirective,
  ],
})
export class HomeModule {}
