import { Group } from "../../../../../interfaces";
import {
  Component,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter,
  HostListener,
  ViewChild,
  ElementRef,
} from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "context-menu",
  template: `
    <div
      #container
      class="container"
      [style.top]="position.y + 'px'"
      [style.left]="position.x + 'px'"
    >
      <!-- Rename -->
      <div class="item" (click)="rename.emit()">
        <i class="fas fa-edit"></i>
        Rename
      </div>
      <!-- Delete -->
      <div class="item" (click)="delete.emit()">
        <i class="fas fa-trash-alt"></i>
        Delete
      </div>
    </div>
  `,
  styleUrls: ["./context-menu.component.sass"],
})
export class ContextMenuComponent {
  @Input() position: { x: number; y: number };
  @Output() exit = new EventEmitter();
  @Output() rename = new EventEmitter();
  @Output() delete = new EventEmitter();

  @HostListener("document:click", ["$event"])
  onClick(e: MouseEvent) {
    this.exit.emit();
  }
}
