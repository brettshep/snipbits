import {
  Directive,
  ElementRef,
  Input,
  Output,
  EventEmitter,
} from "@angular/core";

@Directive({
  selector: "[inputFocus]",
})
export class FocusDirective {
  @Input() set inputValue(val: string) {
    (<HTMLInputElement>this.elem.nativeElement).value = val;
  }

  @Input() set selectFocus(val: boolean) {
    if (val) {
      // enable
      (<HTMLInputElement>this.elem.nativeElement).disabled = false;

      // focus
      (<HTMLInputElement>this.elem.nativeElement).focus();

      //select content
      (<HTMLInputElement>this.elem.nativeElement).select();
    } else {
      // disable
      (<HTMLInputElement>this.elem.nativeElement).disabled = true;
    }
  }
  constructor(private elem: ElementRef) {}

  ngAfterViewInit() {
    // blur event
    (<HTMLInputElement>this.elem.nativeElement).addEventListener(
      "blur",
      (e) => {
        this.onBlur.emit((<HTMLInputElement>this.elem.nativeElement).value);
      }
    );
    (<HTMLInputElement>this.elem.nativeElement).addEventListener(
      "keydown",
      (e: KeyboardEvent) => {
        if (e.code === "Enter" || e.code === "NumpadEnter") {
          (<HTMLInputElement>this.elem.nativeElement).blur();
        }
      }
    );
  }

  @Output() onBlur = new EventEmitter<string>();
}
