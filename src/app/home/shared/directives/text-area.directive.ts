import {
  Directive,
  ElementRef,
  Input,
  Output,
  EventEmitter,
} from "@angular/core";

@Directive({
  selector: "[customTextArea]",
})
export class TextAreaDirective {
  @Input() set initialValue(val: string) {
    this.textarea.value = val;
    // resize
    this.resize();
    // emit resize finished
    this.initialResizeComplete.emit();
  }

  @Output() initialResizeComplete = new EventEmitter<string>();

  @Output() onNewValue = new EventEmitter<string>();

  @Output() onInput = new EventEmitter<string>();

  currHeight: number;
  constructor(private elemRef: ElementRef) {}

  ngAfterViewInit() {
    // setup input resize listener
    this.textarea.addEventListener("input", (e: KeyboardEvent) => {
      // check for enter
      if (e.code !== "Enter" && e.code !== "NumpadEnter") {
        const prevHeight = this.currHeight;
        this.resize();
        if (prevHeight !== this.currHeight)
          this.onInput.emit(this.textarea.value);
      }
    });

    // setup blur listener
    this.textarea.addEventListener("blur", (e: Event) => {
      this.onNewValue.emit(this.textarea.value);
    });

    // setup enter block
    this.textarea.addEventListener("keydown", (e: KeyboardEvent) => {
      if (e.code === "Enter" || e.code === "NumpadEnter") {
        this.textarea.blur();
      }
    });
  }

  resize() {
    if (this.textarea.scrollHeight > this.textarea.clientHeight) {
      this.textarea.style.height = this.textarea.scrollHeight + "px";
    } else {
      this.textarea.style.height = 0 + "px";
      this.textarea.style.height = this.textarea.scrollHeight + "px";
    }

    this.currHeight = this.textarea.clientHeight;
  }

  get textarea() {
    return this.elemRef.nativeElement as HTMLTextAreaElement;
  }
}
