import { Snippet } from "../../../../../interfaces";
import { ElementRef } from "@angular/core";
import {
  Component,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  HostListener,
} from "@angular/core";

import { Subject, Subscription } from "rxjs";
import { debounceTime, distinctUntilChanged } from "rxjs/internal/operators";
import { CopyService } from "../../../services/copy.service";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "snippet-body",
  template: `
    <div class="container" #container [class.active]="snippet">
      <div class="fixedSize" #fixedSize>
        <ngx-codemirror
          *ngIf="showCodeEditor"
          #codeMirror
          [(ngModel)]="content"
          (ngModelChange)="onModelChange($event)"
          [options]="options"
        ></ngx-codemirror>
      </div>
    </div>
  `,
  styleUrls: ["./snippet-body.component.sass"],
})
export class SnippetBodyComponent {
  @Input() set setSnippet(val: Snippet) {
    this.snippet = val;
    if (val) {
      this.options.mode = val.language;
      this.content = val.body ? val.body : this.emptyLines;
    }
  }

  @Input() resizeEditorEmitter: EventEmitter<any>;
  @Input() copyEmitter: EventEmitter<any>;

  @ViewChild("container", { static: true }) contElemRef: ElementRef<
    HTMLDivElement
  >;
  @ViewChild("fixedSize", { static: true }) sizeElemRef: ElementRef<
    HTMLDivElement
  >;
  @ViewChild("codeMirror") codeMirror: any;

  @Output() save = new EventEmitter<{
    propName: string;
    data: any;
    id: string;
  }>();

  emptyLines: string = new Array(40).fill("").reduce((curr) => curr + "\n");
  editorChanged: Subject<string> = new Subject<string>();
  content: string = "";
  copySub: Subscription;
  editorSub: Subscription;
  resizeSub: Subscription;
  snippet: Snippet;
  showCodeEditor: boolean = false;

  options = {
    theme: "material-palenight",
    mode: "text/html",
    // styleActiveLine: false,
    lineNumbers: true,
    lineWrapping: true,
    matchBrackets: true,
    autoCloseBrackets: true,
    extraKeys: {
      "Ctrl-/": "toggleComment",
    },
  };

  constructor(private copyServ: CopyService) {}

  ngOnInit() {
    // sub to resize
    this.resizeSub = this.resizeEditorEmitter.subscribe(() => {
      this.resize();
      if (!this.showCodeEditor) {
        // elements are resized and ready to instantiate
        this.showCodeEditor = true;
      } else {
        this.codeMirror.codeMirror.refresh();
      }
    });

    // sub to copy
    this.copySub = this.copyEmitter.subscribe(() => {
      if (this.showCodeEditor === true) {
        this.copyServ.copy(this.content.trim());
      }
    });
    // sub to model
    this.editorSub = this.editorChanged
      .pipe(debounceTime(1000), distinctUntilChanged())
      .subscribe((model) => {
        this.save.emit({ id: this.snippet.uid, propName: "body", data: model });
      });
  }

  ngOnDestroy() {
    this.editorSub.unsubscribe();
    this.resizeSub.unsubscribe();
    this.copySub.unsubscribe();
  }

  onModelChange(text: string) {
    this.editorChanged.next(text);
  }

  @HostListener("window:resize")
  resize() {
    const contElem = this.contElemRef.nativeElement;
    const sizeElem = this.sizeElemRef.nativeElement;

    const computedStyle = getComputedStyle(contElem);

    const height =
      contElem.clientHeight -
      (parseFloat(computedStyle.paddingTop) +
        parseFloat(computedStyle.paddingBottom));

    const width =
      contElem.clientWidth -
      (parseFloat(computedStyle.paddingLeft) +
        parseFloat(computedStyle.paddingRight));

    sizeElem.style.width = `${width}px`;
    sizeElem.style.height = `${height}px`;
  }
}
