import { Snippet } from "../../../../../interfaces";
import { ElementRef, ChangeDetectorRef } from "@angular/core";
import {
  Component,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter,
} from "@angular/core";
@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "snippet-header",
  template: `
    <div class="container" *ngIf="snippet">
      <div class="top">
        <!-- Title -->
        <form
          (submit)="formNameChangeSubmit($event)"
          (click)="edittingForm = true"
          [class.active]="edittingForm"
        >
          <input
            type="text"
            inputFocus
            [inputValue]="snippet.name"
            [selectFocus]="edittingForm"
            (onBlur)="updateSnippetName($event)"
          />
        </form>
        <!-- dropdown -->
        <div
          class="dropdown"
          [class.open]="languageDropdownOpen"
          (click)="toggleLandDropdown($event)"
        >
          <!-- curr language -->
          <div class="choosen-option" [class.open]="languageDropdownOpen">
            {{ langMap[snippet.language] }}
            <!-- toggle arrow -->
            <svg
              class="toggle-arrow"
              [class.open]="languageDropdownOpen"
              width="11"
              height="10"
              viewBox="0 0 11 10"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M6.25153 8.62216C5.86663 9.28883 4.90438 9.28883 4.51948 8.62216L0.697423 2.00217C0.312523 1.3355 0.793649 0.502171 1.56345 0.502171L9.20756 0.502171C9.97736 0.502171 10.4585 1.3355 10.0736 2.00217L6.25153 8.62216Z"
              />
            </svg>
          </div>
          <!-- other options -->
          <div *ngIf="languageDropdownOpen" class="other-dropdown-options">
            <ng-container *ngFor="let language of languages">
              <div
                *ngIf="language !== snippet.language"
                class="dropdown-option"
                (click)="updateLanguage(language)"
              >
                {{ langMap[language] }}
              </div>
            </ng-container>
          </div>
        </div>

        <!-- copy button -->
        <button class="copy-button action-button" (click)="copyEmitter.emit()">
          <i class="fas fa-copy"></i>
        </button>

        <!-- options/delete dropdown -->
        <div class="options-cont">
          <button
            class="options action-button"
            [class.active]="deleteDrop"
            (click)="toggleDeleteDropdown($event)"
          >
            <i class="far fa-ellipsis-v"></i>
          </button>
          <div
            *ngIf="deleteDrop"
            class="delete-dropdown"
            (click)="deleteSnippet.emit(snippet.uid)"
          >
            <i class="fas fa-trash"></i>Delete
          </div>
        </div>

        <!-- delete dropdown -->
      </div>
      <!-- Description -->
      <textarea
        [class.active]="edittingDescription"
        customTextArea
        [initialValue]="snippet.description"
        (onNewValue)="updateDescription($event)"
        (onInput)="resizeEditorEmitter.emit()"
        (initialResizeComplete)="resizeEditorEmitter.emit()"
        placeholder="Add a description..."
        (focus)="edittingDescription = true"
        (blur)="edittingDescription = false"
      >
      </textarea>
    </div>
  `,
  styleUrls: ["./snippet-header.component.sass"],
})
export class SnippetHeaderComponent {
  @Input() set setSnippet(val: Snippet) {
    this.snippet = val;
    if (val) {
      // select title if necessary
      if (val.name === "Untitled") {
        this.edittingForm = true;
      }
    }
  }
  @Input() resizeEditorEmitter: EventEmitter<any>;
  @Input() copyEmitter: EventEmitter<any>;
  @Output() deleteSnippet = new EventEmitter<string>();
  @Output() onSnippetUpdate = new EventEmitter<{
    propName: string;
    data: any;
    id: string;
  }>();

  snippet: Snippet;
  deleteDrop: boolean = false;
  edittingForm: boolean = false;
  edittingDescription: boolean = false;
  languages = [
    "text/typescript",
    "text/javascript",
    "text/html",
    "text/css",
    "text/x-sass",
  ];

  langMap = {
    "text/javascript": "JS",
    "text/typescript": "TS",
    "text/html": "HTML",
    "text/css": "CSS",
    "text/x-sass": "SASS",
  };

  languageDropdownOpen = false;

  constructor(private cd: ChangeDetectorRef) {}

  toggleLandDropdown(e: Event) {
    this.languageDropdownOpen = !this.languageDropdownOpen;

    // if opening, make sure to close on any click
    if (this.languageDropdownOpen) {
      let first = false;
      const click = () => {
        if (first) {
          this.languageDropdownOpen = false;
          document.removeEventListener("click", click);
          this.cd.detectChanges();
        }
        first = true;
      };

      document.addEventListener("click", click);
    }
  }
  toggleDeleteDropdown(e: Event) {
    this.deleteDrop = !this.deleteDrop;

    // if opening, make sure to close on any click
    if (this.deleteDrop) {
      let first = false;
      const click = () => {
        if (first) {
          this.deleteDrop = false;
          document.removeEventListener("click", click);
          this.cd.detectChanges();
        }
        first = true;
      };

      document.addEventListener("click", click);
    }
  }

  updateSnippetName(name: string) {
    this.edittingForm = false;
    if (name === this.snippet.name) return;

    this.snippet.name = name;
    this.onSnippetUpdate.emit({
      id: this.snippet.uid,
      propName: "name",
      data: name,
    });
  }

  updateDescription(desc: string) {
    if (desc === this.snippet.description) return;

    this.snippet.description = desc;
    this.onSnippetUpdate.emit({
      id: this.snippet.uid,
      propName: "description",
      data: desc,
    });
  }

  updateLanguage(lang: string) {
    this.snippet.language = lang;
    this.onSnippetUpdate.emit({
      id: this.snippet.uid,
      propName: "language",
      data: lang,
    });
  }

  formNameChangeSubmit(e: Event) {
    e.preventDefault();
  }
}
