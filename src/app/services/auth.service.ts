import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import {
  AngularFirestoreDocument,
  AngularFirestore,
} from "@angular/fire/firestore";
import { switchMap, tap } from "rxjs/operators";
import { of } from "rxjs";
import { Store } from "../store";
import { User } from "../../../interfaces";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  UID: string = "";

  constructor(
    private afAuth: AngularFireAuth,
    private fireStore: AngularFirestore,
    private store: Store
  ) {
    this.afAuth.authState
      .pipe(
        switchMap((user) => {
          this.UID = user.uid;
          if (user) {
            return this.fireStore.doc<User>(`users/${user.uid}`).get();
          } else {
            return of(null);
          }
        }),
        tap((snap) => {
          this.store.set("user", snap.data());
        })
      )
      .subscribe();
  }

  LoginUser(email: string, password: string) {
    return this.afAuth.signInWithEmailAndPassword(email, password);
  }

  Logout() {
    this.afAuth.signOut();
  }

  CreateUser(email: string, password: string) {
    return this.afAuth
      .createUserWithEmailAndPassword(email, password)
      .then((credential) => {
        //set default data if new user
        this.SetUserData(credential.user);
      });
  }

  //update user info in Firestore
  private SetUserData(user: firebase.User) {
    const userRef: AngularFirestoreDocument<User> = this.fireStore.doc(
      `users/${user.uid}`
    );
    const data: User = {
      uid: user.uid,
      email: user.email,
      sections: [],
    };
    return userRef.set(data, { merge: true });
  }

  get authState() {
    return this.afAuth.authState;
  }

  get newUID(): string {
    return this.fireStore.createId();
  }
}
