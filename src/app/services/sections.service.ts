import { Injectable } from "@angular/core";
import { AuthService } from "./auth.service";
import { AngularFirestore } from "@angular/fire/firestore";
import { Section } from "../../../interfaces";
import { Store } from "../store";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class SectionsService {
  constructor(
    private authServ: AuthService,
    private fireStore: AngularFirestore,
    private store: Store
  ) {}

  updateSections(sections: Section[]) {
    // update store
    this.store.updateObjectProp("user", { sections });

    // delete folded properties for db
    let newSections = sections.map((section) => {
      const newSection = { ...section, folded: true };
      return newSection;
    });

    // update firestore
    this.fireStore
      .collection("users")
      .doc(this.authServ.UID)
      .update({ sections: newSections });
  }

  get newUID() {
    return this.fireStore.createId();
  }
}
