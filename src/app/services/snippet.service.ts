import { Injectable } from "@angular/core";
import { Store } from "../store";
import { AngularFirestore } from "@angular/fire/firestore";
import { AuthService } from "./auth.service";
import { tap, take, switchMap, map } from "rxjs/operators";
import { Observable, BehaviorSubject } from "rxjs";
import {
  SearchSnippet,
  Snippet,
  User,
  Section,
  Group,
} from "../../../interfaces";

@Injectable({
  providedIn: "root",
})
export class SnippetService {
  choosenCategoryID$: BehaviorSubject<string> = new BehaviorSubject("ALL");

  constructor(
    private authServ: AuthService,
    private fireStore: AngularFirestore,
    private store: Store
  ) {
    // when user id is avaialable, use it to set all searchSnippets to store
    const authSub = this.authServ.authState.subscribe((val) => {
      if (val) {
        this.fireStore
          .collection("users")
          .doc(this.authServ.UID)
          .collection("searchSnippets")
          .get()
          .pipe(
            take(1),
            tap((val) => {
              const data = val.docs.map((doc) => doc.data());
              this.store.set("searchSnippets", data);
            })
          )
          .subscribe();

        authSub.unsubscribe();
      }
    });
  }

  getFilteredSearchSnips(): Observable<SearchSnippet[]> {
    return this.choosenCategoryID$.pipe(
      switchMap((id) => {
        const user = this.store.getDirect<User>("user");
        const sections = user.sections;
        // check if id is section
        let section: Section;

        for (let i = 0; i < sections.length; i++) {
          const _section = sections[i];
          if (_section.uid === id) {
            section = _section;
            // id is section
            break;
          } else {
            for (let j = 0; j < _section.children.length; j++) {
              const child = _section.children[j];
              if (child.uid === id) {
                // id is a group with parent
                break;
              }
            }
          }
        }
        return this.store.select<SearchSnippet[]>("searchSnippets").pipe(
          map((snips) => {
            // check for all
            if (id === "ALL") return snips;
            else {
              const filtered = snips.filter((snip) => {
                // if id is not a section, just retrun snips where id equals their parentID, which will be a group
                if (!section) {
                  return snip.parentID === id;
                }

                // otherwise, we need to loop through the sections groups and if any snip has them as a parent, include them too
                else {
                  // match section
                  if (snip.parentID === id) return true;

                  // match groups
                  for (let i = 0; i < section.children.length; i++) {
                    const groupID = section.children[i].uid;
                    if (snip.parentID === groupID) return true;
                  }
                }
              });
              return filtered;
            }
          })
        );
      })
    );
  }

  createNewSnippet() {
    const parentID = this.choosenCategoryID$.value;

    const currSnip = this.store.getDirect<Snippet>("currSnippet");

    const UID = this.fireStore.createId();

    const snippet: Snippet = {
      uid: UID,
      name: "Untitled",
      description: "",
      language: currSnip ? currSnip.language : "text/typescript",
      body: "",
      parentID,
    };

    const searchSnippet: SearchSnippet = {
      uid: UID,
      name: "Untitled",
      description: "",
      parentID,
    };

    // add to firestore snippets collects
    this.fireStore
      .collection("users")
      .doc(this.authServ.UID)
      .collection("snippets")
      .doc(UID)
      .set(snippet);

    // add to firestore searchsnippets
    this.fireStore
      .collection("users")
      .doc(this.authServ.UID)
      .collection("searchSnippets")
      .doc(UID)
      .set(searchSnippet);

    // add to store (currSnippet)
    this.store.set("currSnippet", snippet);

    // add to store (general snip)
    this.store.set(snippet.uid, snippet);

    // add to store search snippet
    const currAllSnip = this.store.getDirect<SearchSnippet[]>("searchSnippets");

    this.store.set("searchSnippets", [...currAllSnip, searchSnippet]);
  }

  async fetchSnippet(id: string) {
    const storeSnip = await this.store.selectAsPromise(id);

    if (storeSnip) {
      this.store.set("currSnippet", storeSnip);
    } else {
      // fetch from db
      const doc = await this.fireStore
        .collection("users")
        .doc(this.authServ.UID)
        .collection("snippets")
        .doc(id)
        .get()
        .toPromise();

      this.store.set("currSnippet", doc.data());
      this.store.set(id, doc.data());
    }
  }

  updateSnippetData(id: string, propName: string, data: any) {
    // update firebase snippet
    this.fireStore
      .collection("users")
      .doc(this.authServ.UID)
      .collection("snippets")
      .doc(id)
      .update({ [propName]: data });

    // update store currsnippet
    const currSnip = this.store.getDirect<Snippet>("currSnippet");
    if (currSnip)
      this.store.updateObjectProp("currSnippet", { [propName]: data });

    // update store snippet
    const storeSnip = this.store.getDirect<Snippet>(id);
    if (storeSnip) this.store.updateObjectProp(id, { [propName]: data });

    // update firebase searchsnippet
    if (
      propName === "name" ||
      propName === "description" ||
      propName === "parentID"
    ) {
      this.fireStore
        .collection("users")
        .doc(this.authServ.UID)
        .collection("searchSnippets")
        .doc(id)
        .update({ [propName]: data });

      // update store searchSnippets
      const searchSnippets = this.store.getDirect<SearchSnippet[]>(
        "searchSnippets"
      );

      const edittedSnips = searchSnippets.map((snip) => {
        if (snip.uid === id) snip[propName] = data;
        return snip;
      });

      this.store.set("searchSnippets", edittedSnips);
    }
  }

  deleteSnippet(id: string) {
    // remove current snip from store
    this.store.set("currSnippet", null);

    // delete from store searchSnippets
    const searchSnippets = this.store.getDirect<SearchSnippet[]>(
      "searchSnippets"
    );

    const newSearchSnips = searchSnippets.filter((snip) => snip.uid !== id);

    this.store.set("searchSnippets", newSearchSnips);

    // remove snippet from db
    this.fireStore
      .collection("users")
      .doc(this.authServ.UID)
      .collection("snippets")
      .doc(id)
      .delete();

    // remove search snippet from db
    this.fireStore
      .collection("users")
      .doc(this.authServ.UID)
      .collection("searchSnippets")
      .doc(id)
      .delete();
  }

  // changeSnippetParent(snipID:string, target: Section | Group){
  //   // change db snippet
  //   this.fireStore
  //   .collection("users")
  //   .doc(this.authServ.UID)
  //   .collection("snippets")
  //   .doc(snipID)
  //   .update({parent: target.uid})

  //   // change db snippet
  // }
}
