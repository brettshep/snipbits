import { Observable, BehaviorSubject } from "rxjs";
import { pluck, distinctUntilChanged, take } from "rxjs/operators";
import { Injectable } from "@angular/core";
import { User, SearchSnippet, Snippet } from "../../interfaces";

export interface State {
  user: User;
  searchSnippets: SearchSnippet[];
  currSnippet: Snippet;
  [key: string]: any;
}

const state: State = {
  user: undefined,
  currSnippet: undefined,
  searchSnippets: [],
};

@Injectable()
export class Store {
  private subject = new BehaviorSubject<State>(state);
  private store = this.subject.asObservable().pipe(distinctUntilChanged());

  get value() {
    return this.subject.value;
  }

  select<T>(name: string): Observable<T> {
    return this.store.pipe(pluck(name));
  }

  selectAsPromise<T>(name: string): Promise<T> {
    return new Promise((res, rej) => {
      this.store.pipe(pluck(name), take(1)).subscribe((val) => {
        res(val);
      });
    });
  }

  set(name: string, state: any) {
    this.subject.next({ ...this.value, [name]: state });
  }

  getDirect<T>(name: string): T {
    return this.value[name];
  }

  updateObjectProp(objectName: string, data: any) {
    this.subject.next({
      ...this.value,
      [objectName]: { ...this.value[objectName], ...data },
    });
  }

  log() {
    console.log(this.value);
  }
}
